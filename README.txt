== Introduction ==

Json Editor is text formatter for drupal.

== Installation ==

Extract json_editor to your sites/all/modules directory
Enable the Json Editor module in admin/modules

== Using Json Editor ==

Open content type and click on "Manage fields"
Create new field and set field type as "Text long" (text_long) or "Text long and summary" (text_with_summary).
Click on "Manage display" and choose "Json Editor" format for your created field as display.

== Sponsored by ==

This module has been originally developed under the sponsorship of 
the Web Solutions HR (http://websolutions.hr).
